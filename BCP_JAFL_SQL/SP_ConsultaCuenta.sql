-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE Insert_Cuenta
  @cuenta varchar(14),
  @ci char(12),
  @fechaDeRegistro datetime,
  @fechaDeActualizacion datetime

AS
BEGIN

DECLARE @temp table(
  cuenta varchar(14),
  ci char(12),
  fechaDeRegistro datetime,
  fechaDeActualizacion datetime)

INSERT INTO BCP_CUENTA(
	CUENTA,
	CI,
	[FECHA DE REGISTRO],
	[FECHA DE ACTUALIZACION]
)
OUTPUT 
		Inserted.CUENTA
		,Inserted.CI
		,Inserted.[FECHA DE REGISTRO]
		,Inserted.[FECHA DE ACTUALIZACION]
	INTO @temp
VALUES (
  @cuenta,
  @ci,
  @fechaDeRegistro ,
  @fechaDeActualizacion
)
SELECT * FROM @temp
END
GO

