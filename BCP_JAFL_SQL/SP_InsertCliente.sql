CREATE PROCEDURE Insert_Cliente
  @paterno varchar(30),
  @materno varchar(30),
  @nombres varchar(30),
  @ci char(12),
  @fechaDeNacimiento datetime,
  @genero varchar(30),
  @celular int,
  @nivelDeIngresos decimal,
  @correo varchar(30),
  @fechaDeRegistro datetime,
  @fechaDeActualizacion datetime

AS
BEGIN

DECLARE @temp table(
  paterno varchar(30),
  materno varchar(30),
  nombres varchar(30),
  ci char(12),
  fechaDeNacimiento datetime,
  genero varchar(30),
  celular int,
  nivelDeIngresos decimal,
  correo varchar(30),
  fechaDeRegistro datetime,
  fechaDeActualizacion datetime)

INSERT INTO BD_CLIENTES_JAFL.dbo.BCP_CLIENTE(
	[PATERNO]
    ,[MATERNO]
    ,[NOMBRES]
    ,[CI]
    ,[FECHA DE NACIMIENTO]
    ,[GENERO]
    ,[CELULAR]
    ,[NIVEL DE INGRESOS]
    ,[CORREO]
    ,[FECHA DE REGISTRO]
    ,[FECHA DE ACTUALIZACION]
)
OUTPUT 
		Inserted.[PATERNO]
		,Inserted.[MATERNO]
		,Inserted.[NOMBRES]
		,Inserted.[CI]
		,Inserted.[FECHA DE NACIMIENTO]
		,Inserted.[GENERO]
		,Inserted.[CELULAR]
		,Inserted.[NIVEL DE INGRESOS]
		,Inserted.[CORREO]
		,Inserted.[FECHA DE REGISTRO]
		,Inserted.[FECHA DE ACTUALIZACION]
	INTO @temp
VALUES (
  @paterno,
  @materno,
  @nombres,
  @ci,
  @fechaDeNacimiento,
  @genero,
  @celular,
  @nivelDeIngresos,
  @correo,
  @fechaDeRegistro ,
  @fechaDeActualizacion
)
SELECT * FROM @temp
END

EXEC Insert_Cliente 'Foronda', 'Loza', 'Jose Antonio', '9151596', '2024/01/25', 'Male', 69909535, 123.4, 'jaf@gmail.com', '2024/01/25', '2024/01/25';